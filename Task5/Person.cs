﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task5
{
    //  Person class with firstname, lastname and phonenumber
    public class Person
    {
        public string firstname { get; set; }
        public string lastname { get; set; }
        public int telephone { get; set; }

        public Person(string firstname, string lastname)
        {
            this.firstname = firstname;
            this.lastname = lastname;
        }
        //  Overload constructor
        public Person(string firstname, string lastname, int telephone)
        {
            this.firstname = firstname;
            this.lastname = lastname;
            this.telephone = telephone;
        }
        public string Firstname()
        {
            return firstname;
        }
        public string Lastname()
        {
            return lastname;
        }
        public int Telephone()
        {
            return telephone;
        }
    }
}
