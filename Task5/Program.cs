﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task5
{
    //  Program that lets you search through a list of 5 given names
    //  with partial search of both first and last name
    class NameSearcher
    {
        static void Main(string[] args)
        {
            // Creates contactlist using the person class, with an example of the overload constructor on contact4
            //  Demonstration of using creating an enumerable collection
            Console.WriteLine("Enumerable collection");
            Console.WriteLine();
            Person[] contacts = new Person[5]
            {
                new Person("Arnold", "Schwarzenegger", 45201954),
                new Person("Jason", "Statham", 40208829),
                new Person("Steven", "Segal", 90201989),
                new Person("Ferris", "Bueller"),
                new Person("Sandra", "Bullock", 98230139)
            };

            People contactList = new People(contacts);
            foreach (Person p in contactList)
                Console.WriteLine(p.firstname + " " + p.lastname + " " + p.telephone);

            Console.WriteLine("----------------------------------------------------");
            Console.WriteLine("Demonstration of using LINQ queries on a collection");
            Console.WriteLine();

            List<Person> newcontacts = new List<Person>();
            Person contact1 = new Person("Arnold", "Schwarzenegger", 45201954);
            Person contact2 = new Person("Jason", "Statham", 40208829);
            Person contact3 = new Person("Steven", "Segal", 90201989);
            Person contact4 = new Person("Ferris", "Bueller");
            Person contact5 = new Person("Sandra", "Bullock", 98230139);
            //  Adds each contact to the new list
            newcontacts.Add(contact1);
            newcontacts.Add(contact2);
            newcontacts.Add(contact3);
            newcontacts.Add(contact4);
            newcontacts.Add(contact5);

            //  Finds every contact in the list that matches the search
            IEnumerable<Person> searchResult = from contact in newcontacts
                                               where contact.firstname == "Arnold"
                                               select contact;

            // Returns every match
            foreach (Person i in searchResult)
            {
                Console.WriteLine(i.firstname + " " + i.lastname);
            }





            //  Adds each contact to the contactlist
            //contacts.Add(contact1);
            //contacts.Add(contact2);
            //contacts.Add(contact3);
            //contacts.Add(contact4);
            //contacts.Add(contact5);

            //  Allows the user to search through the contacts in the contact list
            //  I couldnt find a way to allow the user to search by the contact's lastname or phonenumber
            //string search = Console.ReadLine();
            //List<Person> matches = contacts.FindAll(contactName=> contactName.Firstname().Contains(search));
            //foreach (Person contact in matches)
            //{
            //    Console.WriteLine(contact.firstname + " " + contact.lastname + " " + contact.telephone);
            //}
        }
    }
}
